package ru.aev.eoc;

public class Main {
    public static void main(String[] args) {
        EggsOrChicken egg = new EggsOrChicken("egg");
        EggsOrChicken chicken = new EggsOrChicken("chicken");

        egg.start();
        chicken.start();

        if (egg.isAlive() || chicken.isAlive()) {
            try {
                egg.join();
                chicken.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
              System.out.print("Первым появилось(ась): " + EggsOrChicken.name.get(0));
        }
    }
}
